# modify-url-parameters

Enables you to modify a URL, add and remove parameters and hashes.

[![Pipeline status](https://gitlab.com/wanjapflueger/modify-url-parameters/badges/master/pipeline.svg)](https://gitlab.com/wanjapflueger/modify-url-parameters/-/pipelines)
![npm](https://img.shields.io/npm/v/@wanjapflueger/modify-url-parameters)
![npm bundle size](https://img.shields.io/bundlephobia/min/@wanjapflueger/modify-url-parameters)

---

## Table of contents

<!-- TOC -->

- [modify-url-parameters](#modify-url-parameters)
  - [Table of contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [Options](#options)
    - [add](#add)
    - [remove](#remove)
    - [changeUrl](#changeurl)
    - [redirect](#redirect)
    - [url](#url)
    - [hash](#hash)
    - [joinSeparator](#joinseparator)
    - [joined](#joined)

<!-- /TOC -->

---

## Install

```
npm i @wanjapflueger/modify-url-parameters
```

## Usage

```js
var modifyUrlParams = require("@wanjapflueger/modify-url-parameters");
modifyUrlParams.modifyUrlParams({
  // ...options
});

// or

import { modifyUrlParams } from "@wanjapflueger/modify-url-parameters";
modifyUrlParams({
  // ...options
});
```

## Options

You will find all type declarations like `Parameter` here: [src/index.ts](https://gitlab.com/wanjapflueger/modify-url-parameters/-/blob/master/src/index.ts).

### add

| Type          | Required |
| ------------- | -------- |
| `Parameter[]` | ❌ No    |

Parameters to add. Paramter names or parameter name and specific parameter values.

```js
modifyUrlParams({
  add: [
    'darkmode',
  ],
  url: 'https://example.com/'
}) // https://example.com/?darkmode=

modifyUrlParams({
  add: [
    {
      color: ['blue', 'yellow']
    }
  ],
  url: 'https://example.com/'
}) // https://example.com/?color=blue&color=yellow
```

### remove

| Type          | Required |
| ------------- | -------- |
| `Parameter[]` | ❌ No    |

Parameters to remove. Paramter names or parameter name and specific parameter values.

```js
modifyUrlParams({
  remove: [
    'darkmode',
  ],
  url: 'https://example.com/?darkmode=true'
}) // https://example.com/

modifyUrlParams({
  remove: [
    {
      color: ['blue']
    }
  ],
  url: 'https://example.com/?color=blue&color=yellow'
}) // https://example.com/?&color=yellow
```

### changeUrl

| Type      | Required |
| --------- | -------- |
| `boolean` | ❌ No    |

Apply changed parameters in the current URL in the browsers address bar if `true`. Only if [url](#url) is `undefined`.

### redirect

| Type      | Required |
| --------- | -------- |
| `boolean` | ❌ No    |

Redirect to updated URL with modified parameters if `true`

### url

| Type     | Required |
| -------- | -------- |
| `string` | ❌ No    |

Any URL. Default is the current URL `window.location.href`

### hash

| Type     | Required |
| -------- | -------- |
| `string` | ❌ No    |

Any hash that will be added to the URL. Will overwrite existing hash.

```js
modifyUrlParams({
  hash: [
    'foo',
  ],
  url: 'https://example.com/'
}) // https://example.com/#foo
```

### joinSeparator

| Type     | Required |
| -------- | -------- |
| `string` | ❌ No    |

The separator will be used to split and join parameter values. Will only join parameter values if [joined](#joined) is `true`.

```js
modifyUrlParams({
  joinSeparator: '+',
  url: 'https://example.com/?color=blue+yellow'
}) // https://example.com/?color=blue&color=yellow

modifyUrlParams({
  joinSeparator: '+',
  joined: true,
  url: 'https://example.com/?color=blue+yellow'
}) // https://example.com/?color=blue+yellow
```

### joined

| Type      | Required |
| --------- | -------- |
| `boolean` | ❌ No    |

Use [joinSeparator](#joinSeparator) to join parameters. The default format of the returned URL changes from `'https://example.com/?color=red&color=blue'` to `'https://example.com/?color=red+blue'` (where `'+'` is the [joinSeparator](#joinSeparator)).

```js
modifyUrlParams({
  joinSeparator: '+',
  joined: true,
  url: 'https://example.com/?color=blue&color=yellow'
}) // https://example.com/?color=blue+yellow

modifyUrlParams({
  joinSeparator: ',',
  joined: true,
  url: 'https://example.com/?color=blue&color=yellow'
}) // https://example.com/?color=blue,yellow
```
