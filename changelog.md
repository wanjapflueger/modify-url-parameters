# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.0.0]

### Added

- Initial release

[unreleased]: https://gitlab.com/wanjapflueger/modify-url-parameters
<!-- [v1.0.1]: https://gitlab.com/wanjapflueger/modify-url-parameters/-/compare/v1.0.1...v1.0.0 -->
[v1.0.0]: https://gitlab.com/wanjapflueger/modify-url-parameters/-/tree/v1.0.0
