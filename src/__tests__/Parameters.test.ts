import { modifyUrlParams } from '../index';

test('Param `add` parameter name as `string`', () => {
  expect(
    modifyUrlParams({
      add: ['darkmode'],
      url: 'https://example.com/',
    }),
  ).toBe('https://example.com/?darkmode=');
});

test('Param `add` parameter name as `number`', () => {
  expect(
    modifyUrlParams({
      add: [99],
      url: 'https://example.com/',
    }),
  ).toBe('https://example.com/?99=');
});

test('Param `add` parameter name and `string` value', () => {
  expect(
    modifyUrlParams({
      add: [
        {
          darkmode: ['true'],
        },
      ],
      url: 'https://example.com/',
    }),
  ).toBe('https://example.com/?darkmode=true');
});

test('Param `add` parameter name and `number` value', () => {
  expect(
    modifyUrlParams({
      add: [
        {
          darkmode: [1],
        },
      ],
      url: 'https://example.com/',
    }),
  ).toBe('https://example.com/?darkmode=1');
});

test('Param `add` parameter name and `string` values', () => {
  expect(
    modifyUrlParams({
      add: [
        {
          category: ['foo', 'bar'],
        },
      ],
      url: 'https://example.com/',
    }),
  ).toBe('https://example.com/?category=foo&category=bar');
});

test('Param `add` parameter name and `number` values', () => {
  expect(
    modifyUrlParams({
      add: [
        {
          category: [1, 2],
        },
      ],
      url: 'https://example.com/',
    }),
  ).toBe('https://example.com/?category=1&category=2');
});

test('Param `add` parameter name and `number | string` values', () => {
  expect(
    modifyUrlParams({
      add: [
        {
          category: [1, 'foo'],
        },
      ],
      url: 'https://example.com/',
    }),
  ).toBe('https://example.com/?category=1&category=foo');
});

test('Param `remove` parameter name as `string`', () => {
  expect(
    modifyUrlParams({
      remove: ['darkmode'],
      url: 'https://example.com/?darkmode=',
    }),
  ).toBe('https://example.com/');
});

test('Param `remove` parameter name as `number`', () => {
  expect(
    modifyUrlParams({
      remove: [99],
      url: 'https://example.com/?99=',
    }),
  ).toBe('https://example.com/');
});

test('Param `remove` parameter `string` value', () => {
  expect(
    modifyUrlParams({
      remove: [
        {
          color: ['red'],
        },
      ],
      url: 'https://example.com/?color=red&color=blue',
    }),
  ).toBe('https://example.com/?color=blue');
});

test('Param `remove` parameter `number` value', () => {
  expect(
    modifyUrlParams({
      remove: [
        {
          category: [99],
        },
      ],
      url: 'https://example.com/?category=99&category=123',
    }),
  ).toBe('https://example.com/?category=123');
});

test('Param `remove` parameter `number | string` value', () => {
  expect(
    modifyUrlParams({
      remove: [
        {
          category: [99, 'food'],
        },
      ],
      url: 'https://example.com/?category=food&category=99',
    }),
  ).toBe('https://example.com/');
});

test('Param `remove` runs after param `add`', () => {
  expect(
    modifyUrlParams({
      add: [
        {
          darkmode: ['true'],
        },
      ],
      remove: ['darkmode'],
      url: 'https://example.com/?darkmode=false',
    }),
  ).toBe('https://example.com/');
});

test("Param `joinSeparator` as `'+'` and `joined`", () => {
  expect(
    modifyUrlParams({
      url: 'https://example.com/?color=red&color=blue',
      joinSeparator: '+',
      joined: true,
    }),
  ).toBe('https://example.com/?color=blue+red');
});

test("Param `joinSeparator` as `','` and `joined`", () => {
  expect(
    modifyUrlParams({
      url: 'https://example.com/?color=red&color=blue',
      joinSeparator: ',',
      joined: true,
    }),
  ).toBe('https://example.com/?color=blue,red');
});

test('Param `separator` is required for param `joined` to work', () => {
  try {
    modifyUrlParams({
      url: 'https://example.com/?color=red&color=blue',
      joined: true,
    });
    // Fail test if above expression doesn't throw anything.
    expect(true).toBe(false);
  } catch (e) {
    expect(e.message).toBe("Missing required argument 'options.joinSeparator'");
  }
});

test('Param `joinSeparator` splits the URL with a separator', () => {
  expect(
    modifyUrlParams({
      url: 'https://example.com/?color=blue+red',
      joinSeparator: '+',
    }),
  ).toBe('https://example.com/?color=blue&color=red');
});

test('Param `changeUrl` and `url` is not allowed simultaneously', () => {
  try {
    modifyUrlParams({
      url: 'https://example.com/?color=red&color=blue',
      changeUrl: true,
    });
    // Fail test if above expression doesn't throw anything.
    expect(true).toBe(false);
  } catch (e) {
    expect(e.message).toBe(
      "Combining 'options.changeUrl' with 'options.url' is not allowed",
    );
  }
});

test('Param `hash` is appended to URL', () => {
  expect(
    modifyUrlParams({
      hash: 'foo',
      url: 'https://example.com/',
    }),
  ).toBe('https://example.com/#foo');
});

test('Complex call without param `joinSeparator`', () => {
  expect(
    modifyUrlParams({
      add: [
        99,
        {
          darkmode: ['true'],
          color: ['orange'],
        },
      ],
      remove: [
        'banana',
        {
          color: ['blue'],
        },
      ],
      hash: 'main',
      url: 'https://example.com/?banana=&color=red&color=blue&color=yellow',
    }),
  ).toBe(
    'https://example.com/?99=&color=red&color=yellow&color=orange&darkmode=true#main',
  );
});

test('Complex call with param `joined`', () => {
  expect(
    modifyUrlParams({
      add: [
        99,
        {
          darkmode: ['true'],
          color: ['orange'],
        },
      ],
      remove: [
        'banana',
        {
          color: ['blue'],
        },
      ],
      hash: 'main',
      joinSeparator: '+',
      joined: true,
      url: 'https://example.com/?banana=&color=red&color=blue&color=yellow',
    }),
  ).toBe('https://example.com/?99&color=orange+red+yellow&darkmode=true#main');
});
