/** Parameter name for {@link Parameter}. Represents a `$_GET` paramter name. The part that comes before `'='` in `'/?foo=bar'` → `'foo'`). */
type ParameterName = number | string;

/** Parameter value for {@link Parameter}. Represents a `$_GET` paramter value. The part that comes after `'='` in `'/?foo=bar'` → `'bar'`). */
type ParameterValue = number | string | undefined;

/** A parameter with associated values. */
interface ParameterWithValues {
  [key: string]: ParameterValue[];
}

/** A parameter consists of a parameter name or a parameter name and associated values. */
type Parameter = ParameterName | ParameterWithValues;

/** Options */
interface Options {
  /** Parameters to add. Paramter names or parameter name and specific parameter values. */
  add?: Parameter[];

  /** Parameters to remove. Paramter names or parameter name and specific parameter values. */
  remove?: Parameter[];

  /** Apply changed parameters in the current URL in the browsers address bar if `true`. Only if {@link url} is `undefined`. */
  changeUrl?: boolean;

  /** Redirect to updated URL with modified parameters if `true` */
  redirect?: boolean;

  /** Any URL. Default is the current URL `window.location.href` */
  url?: string;

  /** Any hash that will be added to the URL. Will overwrite existing hash. */
  hash?: string;

  /** Separator. Will be used to split and join parameter values. Will only join parameter values if {@link joined} is `true`. */
  joinSeparator?: string;

  /** Use {@link joinSeparator} to join parameters. The default format of the returned URL changes from `'https://example.com/?color=red&color=blue'` to `'https://example.com/?color=red+blue'` (where `'+'` equals {@link joinSeparator}). */
  joined?: boolean;
}

/**
 * Modify `$_GET` parameters in any URL. You can add and then remove parameters or parameter values. You can change the URL in the addressbar and redirect to the updated URL.
 * @param options Options
 * @returns The URL with updated parameters. Only if {@link Options.redirect} is not `true`. Returns either the `URLSearchParams` format (`'https://example.com/?color=red&color=blue'`) or a shorter joined format (`'https://example.com/?color=red+blue'`).
 * @author Wanja Friedemann Pflüger
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/}
 * @example
 *   modifyUrlParams({
 *     add: [
 *       'darkmode',
 *     ],
 *     url: 'https://example.com/'
 *   }) // https://example.com/?darkmode=
 *
 *   modifyUrlParams({
 *     remove: [
 *       'darkmode',
 *     ],
 *     url: 'https://example.com/?darkmode='
 *   }) // https://example.com/
 *
 *   modifyUrlParams({
 *     add: [
 *       {
 *         darkmode: ['true']
 *       }
 *     ],
 *     remove: [
 *       {
 *         darkmode: ['false']
 *       }
 *     ],
 *     url: 'https://example.com/?darkmode=false'
 *   }) // https://example.com/?darkmode=true
 *
 *   modifyUrlParams({
 *     url: 'https://example.com/?color=red&color=blue',
 *     joinSeparator: '+',
 *     joined: true
 *   }) // https://example.com/?color=blue+red
 *
 *   modifyUrlParams({
 *     url: 'https://example.com/?color=blue+red',
 *     joinSeparator: '+'
 *   }) // https://example.com/?color=blue&color=red
 *
 *   modifyUrlParams({
 *     add: [
 *       'darkmode',
 *       {
 *         category: [99],
 *         color: ['red'],
 *       }
 *     ],
 *     remove: [
 *       'banana',
 *       {
 *         category: [2],
 *       }
 *     ],
 *     joinSeparator: '+',
 *     joined: true,
 *     redirect: false,
 *     changeUrl: false,
 *     hash: 'main',
 *     url: 'https://example.com/?category=1+2+3&banana'
 *   }) // https://example.com/?category=1+3+99&darkmode&color=red#main
 */
export const modifyUrlParams = (options: Options): string => {
  /** The search parameters */
  let searchParams: URLSearchParams;

  /**
   * Parameter value
   * @example
   *   'red' | 'blue' | 99
   */
  let value: ParameterValue = '';

  /**
   * Parameter values
   * @example
   *   ['red', 'blue', 99]
   */
  let values: ParameterValue[] = [value];

  /**
   * Parameter name
   * @example
   *   'color'
   */
  let name: ParameterName = '';

  /**
   * Parameter name and parameter value
   */
  let pair: [ParameterName, ParameterValue] = [name, value];

  /**
   * Parameter name or parameter name and parameter values
   */
  let addAndRemoveFormat: Parameter = name || { [name]: [value] };

  /** Parameter pairs */
  let entries: [ParameterName, ParameterValue][] = [];

  /**
   * URL
   * @example
   *   'https://example.com/?color=red&color=blue' | 'https://example.com/?color=red+blue'
   */
  let url: string | URL = '';

  const typeChecking = () => {
    if (options.joined && !options.joinSeparator) {
      throw Error(`Missing required argument 'options.joinSeparator'`);
    }

    if (options.changeUrl && options.url) {
      throw Error(
        `Combining 'options.changeUrl' with 'options.url' is not allowed`,
      );
    }
  };

  const handleDefaults = () => {
    /** Get the search parameters from {@link options.url} */
    searchParams = new URLSearchParams(
      new URL(options.url ?? window.location.href).search,
    );
  };

  /** Add existing parameters from {@link url} to {@link entries} */
  const handleExisting = () => {
    for (pair of searchParams.entries()) {
      name = pair[0].toString();

      if (name.length) {
        if (options.joinSeparator) {
          /** In {@link searchParams} a `'+'` is interpreted as a space. */
          const separator =
            options.joinSeparator === '+' ? ' ' : options.joinSeparator;

          values =
            pair[1] && !Array.isArray(pair[1])
              ? pair[1].toString().split(separator)
              : [];

          for (value of values) {
            entries.push([name, value]);
          }
        } else {
          value = pair[1];
          entries.push([name, value]);
        }
      }
    }
  };

  /** Add new parameters in {@link options.add} to {@link entries} */
  const handleAdd = () => {
    if (options.add) {
      for (addAndRemoveFormat of options.add) {
        if (
          typeof addAndRemoveFormat === 'string' ||
          typeof addAndRemoveFormat === 'number'
        ) {
          /** {@link addAndRemoveFormat} is a {@link ParameterName} and has no {@link ParameterValues} */
          name = addAndRemoveFormat.toString();
          value = '';

          entries.push([name, value]);
        } else {
          /** {@link addAndRemoveFormat} has {@link ParameterValues} */
          Object.entries(addAndRemoveFormat).forEach((x) => {
            name = x[0];
            values = x[1];

            for (value of values) {
              entries.push([name, value]);
            }
          });
        }
      }

      /** Merge {@link entries} with {@link searchParams} */
      for (pair of entries) {
        name = pair[0].toString();
        value = pair[1]?.toString() || '';

        if (name.length && !searchParams.getAll(name).includes(value)) {
          searchParams.append(name, value);
        }
      }
    }
  };

  /** Remove existing parameters in {@link options.remove} from {@link entries} */
  const handleRemove = () => {
    if (options.remove) {
      for (addAndRemoveFormat of options.remove) {
        if (
          typeof addAndRemoveFormat === 'string' ||
          typeof addAndRemoveFormat === 'number'
        ) {
          /** {@link addAndRemoveFormat} is a {@link ParameterName} and has no {@link ParameterValues} */
          name = addAndRemoveFormat.toString();
          value = '';

          /** Delete {@link name} from {@link searchParams} */
          for (pair of searchParams.entries()) {
            if (searchParams.has(name)) {
              searchParams.delete(name);
            }
          }

          /** Merge {@link searchParams} with {@link entries} */
          entries = [];
          for (pair of searchParams.entries()) {
            entries.push(pair);
          }
        } else {
          /** {@link addAndRemoveFormat} has {@link ParameterValues} */
          Object.entries(addAndRemoveFormat).forEach((y) => {
            name = y[0].toString();
            values = y[1];

            for (value of values) {
              entries.forEach((entry, i) => {
                /** @type {ParameterName} */
                const entryName = entry[0].toString();

                /** @type {ParameterValue} */
                const entryValue = entry[1]?.toString() || '';

                if (name === entryName) {
                  if (value && value?.toString() === entryValue) {
                    entries.splice(i, 1);
                  }
                }
              });
            }
          });
        }
      }
    }
  };

  /** Build the new URL from {@link searchParams} */
  const handleNewUrl = () => {
    /** Build new URL */
    const newUrl = new URL(options.url || window.location.href);
    /** Add `protocol`, `host` and `pathname` */
    url = newUrl.protocol + '//' + newUrl.host + newUrl.pathname;

    if (searchParams.toString().includes('=')) {
      url += '?';

      if (options.joined && options.joinSeparator) {
        const obj: ParameterWithValues = {};

        /** Join parameter values to form a {@link url} like `'https://example.com/?color=red+blue'` */
        searchParams
          .toString()
          .split('&')
          .forEach((x) => {
            const parts = x.split('=');
            name = parts[0].toString();
            value = parts[1].toString();

            if (!obj[name]) {
              obj[name] = [];
            }

            if (value) {
              obj[name].push(value);
            }

            obj[name] = Array.from(new Set(obj[name])).sort();
          });

        Object.entries(obj).forEach((x, i) => {
          name = x[0];
          values = x[1];

          if (i > 0) {
            url += '&';
          }

          if (values && values.length) {
            url += `${name}=${values.join(options.joinSeparator)}`;
          } else {
            url += `${name}`;
          }
        });
      } else {
        /** Convert {@link searchParams} to a `string` to form a {@link url} like `'https://example.com/?color=red&color=blue'` */
        searchParams.sort();
        url += searchParams.toString();
      }
    }

    if (options.hash) {
      /** Append hash to URL */
      url += `#${options.hash}`;
    }
  };

  /** Handle changes that should me made to the current browser state */
  const handleBrowserState = () => {
    if (typeof url === 'string') {
      if (options.changeUrl && !options.url) {
        /** Change URL in the browsers address bar to {@link url} */
        window.history.pushState({ path: url }, '', url);
      }

      if (options.redirect) {
        /** Redirect to {@link url} */
        window.location.href = url;
      }
    }
  };

  /** Merge {@link entries} with {@link searchParams} */
  const mergeParams = () => {
    searchParams = new URLSearchParams();
    for (pair of entries) {
      name = pair[0].toString();
      value = pair[1]?.toString() || '';

      if (name.length && !searchParams.getAll(name).includes(value)) {
        searchParams.append(name, value);
      }
    }
  };

  typeChecking();
  handleDefaults();
  handleExisting();
  mergeParams();
  handleAdd();
  handleRemove();
  mergeParams();
  handleNewUrl();
  handleBrowserState();

  return url;
};
